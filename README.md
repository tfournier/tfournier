# Bonjour à tous,

Je m'appelle Thomas FOURNIER, administrateur R&S et développeur depuis 2012,
j'aime combiner ces aspects de l'informatique, un peu comme un couteau Suisse,
qui grâce à cela, me permet de gérer des projets de A à Z.

J'ai souvent été désigné référent technique, sur les projets/entreprises dans lesquelles j'ai évolué.

Mon expertise porte sur le développement Golang, l'architecture cloud, la culture DevOps, mais aussi dans l'accompagnement technique.


## Me contacter

[![LinkedIn](https://img.shields.io/badge/@tfournier59-0A66C2?logo=linkedin&logoColor=white&style=for-the-badge)](https://www.linkedin.com/in/tfournier59/)
[![GitLab](https://img.shields.io/badge/@tfournier-FCA121?logo=gitlab&style=for-the-badge)](https://gitlab.com/tfournier)
[![MinIO](https://img.shields.io/badge/@tfournier-FC5656?logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAmCAYAAACoPemuAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAIKSURBVHgBzZjxVcIwEIevPv+XEdhAR8AJZAO6AWwATCAbdATdADZAJzicADb4eWdbKaVNLm1p+d67B88052eSXhKJWgIglthKsMRe4kPihYZEBN5Rz4KGIBspF0eJETXkgZrz5mlXqZi6RtdJae0kEuNC+zQblTq49Pwky8FZXOSzSs1qfqkmnJbkjwapJerlbS+KJoSfmUOOS1KJJ5c+71+LhkQuOQ6UyvG/wXCvG5/cuIGUkljEGGHMWox6zsYitkEYjMIaaSClTMoejxVuav9EdtZRFJ1yKQqvXTvpv6Nb0XCkGKG17G6kcFmZzQnRgZR8n6NqJ4A9Od9AallqX+QNvlNCn1I5Y23cww/3KKUsCPcnpSS+LYgHkPoX2xoTJuhHSonzMnFPUud+8mWB85QOLfV3cIwKnTXRhNK965BLUfjed5B4LeTQE+/c8bzusz8SXxKf+b5bC9xXM9NIdU7A0N9MKqqQGssHUxgHupy+JdmXwCnr653CBGEwrq9poZjO/ByQkFExffKzFcK4OvO3uYkf6HL69OiSvupRtJKPNXWJ8a9lVNepIwoX2ICRiy1iI7ins04KDeXsLxrS2zhXJNnCVtHLctOafHs0KTFID5FJFpNS2xJujhV9ivliugWw3T+n1Dfw16vQIt2p3Moxja3+DxtRS5AuXq3cz5RuL98SG+8W4+EXRNU/lYR5jwEAAAAASUVORK5CYII=&logoColor=white&style=for-the-badge)](https://www.malt.fr/profile/tfournier)


## Technologies

![Golang](https://img.shields.io/badge/golang-00ADD8?logo=go&logoColor=white&style=for-the-badge)
![Git](https://img.shields.io/badge/git-3E2C00?logo=git&style=for-the-badge)
![MongoDB](https://img.shields.io/badge/mongodb-47A248?logo=mongodb&logoColor=white&style=for-the-badge)
![Redis](https://img.shields.io/badge/redis-DC382D?logo=redis&logoColor=white&style=for-the-badge)
![MinIO](https://img.shields.io/badge/minio-C72E49?logo=data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzOCIgaGVpZ2h0PSI3MiIgZmlsbD0iI2ZmZiIgeG1sbnM6dj0iaHR0cHM6Ly92ZWN0YS5pby9uYW5vIj48cGF0aCBkPSJNMjguMDUgMy41MWw5LjQ0IDE1LjQ1YS4xOC4xOCAwIDAgMSAwIC4yMS4xNy4xNyAwIDAgMS0uMjUgMEwyNS4wMSA2LjQybDMuMDQtMi45MXoiLz48cGF0aCBkPSJNNy4yIDQ1LjFhNDIgNDIgMCAwIDEgOC4zMi0xMS44IDQyLjYgNDIuNiAwIDAgMSA0LjE2LTMuNjV2OUw3LjIgNDUuMXpNMCA1My40NGwxOS42OC0xMHYyMi45bDQuNDMgNS43NnYtMzFsMi42OS0xLjM5YTEzLjM1IDEzLjM1IDAgMCAwIDMuNjctMjEuMTJMMjAuMzQgOGEyLjI1IDIuMjUgMCAwIDEgLjExLTMuMTcgMi4yNiAyLjI2IDAgMCAxIDMuMTguMTFsMS40MiAxLjQ4IDMtMi45MmMtMy41OS00LjY2LTgtNC4wNy0xMC41My0xLjc0YTYuNDYgNi40NiAwIDAgMC0uMjggOS4xMmwxMC4yNCAxMC42N2E5LjE0IDkuMTQgMCAwIDEtMiAxNC4wOGwtMS4zOS43MlYyMS44OEE0Ni4zMyA0Ni4zMyAwIDAgMCAwIDUzLjM5di4wNXoiLz48cGF0aCBkPSJNMjQuMTEgNDEuMDl2NC42OGwtNC40MyAyLjI1di00LjY1eiIvPjwvc3ZnPg==&logoColor=white&style=for-the-badge)
![MySQL](https://img.shields.io/badge/mysql-4479A1?logo=mysql&logoColor=white&style=for-the-badge)
![GitLab](https://img.shields.io/badge/gitlab-FCA121?logo=gitlab&style=for-the-badge)
![Kubernetes](https://img.shields.io/badge/kubernetes-326CE5?logo=kubernetes&logoColor=white&style=for-the-badge)
![Helm](https://img.shields.io/badge/helm-0F1689?logo=helm&logoColor=white&style=for-the-badge)
![Docker](https://img.shields.io/badge/docker-2496ED?logo=docker&logoColor=white&style=for-the-badge)
![Grafana](https://img.shields.io/badge/grafana-F46800?logo=grafana&logoColor=white&style=for-the-badge)
![Prometheus](https://img.shields.io/badge/prometheus-E6522C?logo=prometheus&logoColor=white&style=for-the-badge)
![TraefikProxy](https://img.shields.io/badge/traefikproxy-24A1C1?logo=traefikproxy&logoColor=white&style=for-the-badge)
![TOML](https://img.shields.io/badge/toml-black?logo=toml&logoColor=white&style=for-the-badge)
![JSON](https://img.shields.io/badge/json-black?logo=json&logoColor=white&style=for-the-badge)
![YAML](https://img.shields.io/badge/yaml-black?logo=yaml&logoColor=white&style=for-the-badge)
![MarkDown](https://img.shields.io/badge/markdown-black?logo=markdown&logoColor=white&style=for-the-badge)
![HTML5](https://img.shields.io/badge/-HTML5-E34F26?logo=html5&logoColor=white&style=for-the-badge)
![CSS3](https://img.shields.io/badge/-CSS3-1572B6?logo=css3&logoColor=white&style=for-the-badge)
![Bootstrap](https://img.shields.io/badge/-Bootstrap-563D7C?logo=bootstrap&logoColor=white&style=for-the-badge)
![Hugo](https://img.shields.io/badge/hugo-FF4088?logo=hugo&logoColor=white&style=for-the-badge)
![OVH](https://img.shields.io/badge/ovh-123F6D?logo=ovh&logoColor=white&style=for-the-badge)
![Scaleway](https://img.shields.io/badge/scaleway-4F0599?logo=scaleway&logoColor=white&style=for-the-badge)
![GoogleCloud](https://img.shields.io/badge/google&nbsp;cloud-4285F4?logo=googlecloud&logoColor=white&style=for-the-badge)
![DigitalOcean](https://img.shields.io/badge/digitalocean-0080FF?logo=digitalocean&logoColor=white&style=for-the-badge)
![CloudFlare](https://img.shields.io/badge/cloudflare-F38020?logo=cloudflare&logoColor=white&style=for-the-badge)
![MacOS](https://img.shields.io/badge/macos-black?logo=macos&logoColor=white&style=for-the-badge)
![Debian](https://img.shields.io/badge/debian-A81D33?logo=debian&logoColor=white&style=for-the-badge)
![Ubuntu](https://img.shields.io/badge/ubuntu-E95420?logo=ubuntu&logoColor=white&style=for-the-badge)
![Ansible](https://img.shields.io/badge/ansible-EE0000?logo=ansible&logoColor=white&style=for-the-badge)
![JetBrains](https://img.shields.io/badge/JetBrains-black?logo=jetbrains&logoColor=white&style=for-the-badge)
